package net.logan.register;

import net.logan.register.accounts.Account;
import net.logan.register.tickets.Ticket;

public class Core {

	private String type;
	private Account acc;
	private Ticket currentTicket;
	private String tempItemName;
	
	public Core()
	{
		
	}
	
	public void setType(String t)
	{
		this.type = t;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public boolean isHost()
	{
		return this.type.equalsIgnoreCase("host");
	}
	
	public boolean isClient()
	{
		return this.type.equalsIgnoreCase("client");
	}
	
	public void setCurrentUser(Account a)
	{
		this.acc = a;
	}
	
	public Account getCurrentUser()
	{
		return this.acc;
	}

	public Ticket getCurrentTicket() {
		return currentTicket;
	}

	public void setCurrentTicket(Ticket currentTicket) {
		this.currentTicket = currentTicket;
	}

	public String getTempItemName() {
		return tempItemName;
	}

	public void setTempItemName(String tempItemName) {
		this.tempItemName = tempItemName;
	}
}
