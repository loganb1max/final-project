package net.logan.register.items;

import java.text.DecimalFormat;

import net.logan.register.Main;

public class Item {

	private String name;
	private double price;
	
	public Item(String n, double d)
	{
		this.name = n;
		this.price = d;
		
	}
	
	public void setName(String n)
	{
		this.name = n;
	}
	
	public void setPrice(double d)
	{
		this.price = d;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public double getPrice()
	{
		return this.price;
	}
	
	public String toString()
	{
		DecimalFormat format = new DecimalFormat("0.00") ;
		return this.getName() + " $" + format.format(this.getPrice());
	}
	
	public void save()
	{
		Main.itemMan.saveItem(this);
	}
}
