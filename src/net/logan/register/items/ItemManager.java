package net.logan.register.items;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.logan.register.ConnectionPool;

public class ItemManager {

	private List<Item> allItems = new ArrayList<Item>();
	private List<String> itemNames = new ArrayList<String>();
	
	public ItemManager() 
	{
		loadSQLItems();
		fillNames();
		
	}
	
	private void fillNames() {
		itemNames.clear();
		for (Item i : allItems)
		{
			itemNames.add(i.getName());
		}
	}
	
	public List<String> getNames()
	{
		return itemNames;
	}

	public void addItem(Item i)
	{
		this.allItems.add(i);
		saveItem(i);
	}
	
	public void displayItems()
	{
		for (Item i : allItems)
		{
			System.out.println(i.toString());
		}
	}
	
	public Item getItem(String name)
	{
		for(Item i : allItems)
		{
			if (i.getName().equals(name))
			{
				return i;
			}
		}
		return null;
	}
	
	public List<Item> getAllItems()
	{
		return this.allItems;
	}
	
	public void loadSQLItems()
	{
		allItems.clear();
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT name, price FROM Items order by name");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next())
			{				
				allItems.add(new Item(rs.getString("name"), rs.getDouble("price")));
			}
			
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		System.out.println("SQL Items loaded!");
		return;
	}
	
	
	public void saveItem(Item i)
	{
		String name = i.getName();
		double p = i.getPrice();
		
		PreparedStatement pst = null;
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO Items "
														+ "(name, price) "
														+ "VALUES('" + name
														+ "', '" + p
														+ "') "
														+ "ON DUPLICATE KEY UPDATE name='" + name
														+"', price='" + p + "'");
		
		pst.executeUpdate();
	} catch (SQLException e) {e.printStackTrace();
	}finally{}
		System.out.println("Item Saved");
		loadSQLItems();	
	}
	
	public void deleteItem(Item i)
	{
		PreparedStatement pst = null;
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `Register`.`Items` WHERE `Items`.`name` = \'" + i.getName() + "\'");
			pst.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally
		{
			
		}
		System.out.println("Account " + i.getName() + " Deleted!");
		allItems.remove(i);
	}
	
}
