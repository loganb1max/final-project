package net.logan.register.tickets;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.logan.register.ConnectionPool;
import net.logan.register.Main;
import net.logan.register.accounts.Account;
import net.logan.register.items.Item;

public class TicketManager {
	
	private List<Ticket> allTickets = new ArrayList<Ticket>();

	public List<Ticket> getAllTickets() {
		return allTickets;
	}

	public void setAllTickets(List<Ticket> allTickets) {
		this.allTickets = allTickets;
	}

	public TicketManager()
	{
		loadAllTickets();
	}
	
	public  Ticket getTicket(int num)
	{
		
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT num, host, items, completed FROM Tickets WHERE num = '"
									+ num + "'");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			if (!(rs.next())) {
				return null;
			}
		
		int oNum = rs.getInt("num");
		String host = rs.getString("host");
		String itemsString = rs.getString("items");
		boolean comp = rs.getBoolean("completed");
		ArrayList<Item> items = new ArrayList<Item>();
		for (String s : itemsString.substring(1, itemsString.length()-1).split(", "))
		{
			for (Item i : Main.itemMan.getAllItems())
			{
				String[] sSplit = s.split(" ");
				if (sSplit[0].equalsIgnoreCase(i.getName()))
				{
					items.add(i);
				}
					
			}
		}
		
		Account h = Main.accMan.getAccount(host);
		
		Ticket t = new Ticket(h);
		for (Item i : items)
		{
			t.add(i);
		}
		t.setNum(oNum);
		t.setCompleted(comp);
		return t;	
		
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		return null;
	}
	
	public void saveTicket(Ticket t)
	{
		int num = t.getNum();
		String host = t.getHost().getUserName();
		String items = t.getItems().toString();
		boolean comp = t.isCompleted();
		int compI = 0;
		if (comp == true)
		{
			compI = 1;
		}
		if (comp == false)
		{
			compI = 0;
		}
		
		PreparedStatement pst = null;
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO Tickets "
														+ "(num, host, items, completed) "
														+ "VALUES('" + num
														+ "', '" + host
														+ "', '"+ items
														+ "', '"+ compI
														+ "') "
														+ "ON DUPLICATE KEY UPDATE num='" + num
														+"', host='" + host
														+ "', items='" + items  
														+ "', completed='" + compI  + "'");
		
		pst.executeUpdate();	
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{
		
		}
	}

	@SuppressWarnings("null")
	public static int getCurrentNum() {
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT currentNum FROM general");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			if (!(rs.next())) {
				return (Integer) null;
			}
		
		int id = rs.getInt("currentNum");
		
		return id;	
		
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		return (Integer) null;
	}
	
	@SuppressWarnings("static-access")
	public static void updateCurrentNum()
	{
		int oldid = Main.accMan.getCurrentID();
		int oldnum= Main.tickMan.getCurrentNum();
		@SuppressWarnings("unused")
		int id = oldid + 1;
		int num = oldnum +1;

		PreparedStatement pst = null;
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `Register`.`general` WHERE `general`.`currentNum` = " + oldid);
			pst.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally
		{
			
		}
		
		
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO general "
					+ "(currentID, currentNum) "
					+ "VALUES('" + oldid
					+ "', '" + num
					+ "') "
					+ "ON DUPLICATE KEY UPDATE currentID='" + oldid
					+"', currentNum='" + num + "'");
		
		pst.executeUpdate();	
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{
		
		}
	}
	
	
	public void loadAllTickets()
	{
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT num FROM Tickets order by num");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next())
			{				
				allTickets.add(getTicket(rs.getInt("num")));	
			}
			
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		System.out.println("Tickets loaded!");
	}
	
	
	
	
	
	
}
