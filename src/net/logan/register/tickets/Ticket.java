package net.logan.register.tickets;

import java.util.ArrayList;
import java.util.List;

import net.logan.register.accounts.Account;
import net.logan.register.items.Item;

public class Ticket {

	private List<Item> items = new ArrayList<Item>();
	private double subTotal;
	private double total;
	private Account host;
	private int ticketNum;
	private boolean completed;
	
	public Ticket(Account user)
	{
		this.host = user;
		ticketNum = TicketManager.getCurrentNum();
		TicketManager.updateCurrentNum();
	}
	
	public void update()
	{
		subTotal = 0;
		for (Item i : items)
		{
			subTotal += i.getPrice();
		}
		double totalTax = subTotal * 0.06;
		setTotal(subTotal + totalTax);
	}
	
	public void add(Item i)
	{
		items.add(i);
		update();
	}
	
	public Account getHost()
	{
		return this.host;
	}
	
	public int getNum()
	{
		return ticketNum;
	}
	
	public List<Item> getItems()
	{
		return this.items;
	}
	
	public void setNum(int i)
	{
		this.ticketNum = i;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
