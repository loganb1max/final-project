package net.logan.register;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Component;

import net.logan.register.gui.accountManagerGUI;
import net.logan.register.gui.clientGUI;
import net.logan.register.gui.hostGUI;
import net.logan.register.gui.itemManagerGUI;
import net.logan.register.gui.newItemNameGUI;
import net.logan.register.gui.newItemPriceGUI;
import net.logan.register.gui.newUserGUI;
import net.logan.register.gui.newUserPassGUI;
import net.logan.register.gui.passGUI;
import net.logan.register.gui.permissionsGUI;
import net.logan.register.gui.typeGUI;
import net.logan.register.gui.userGUI;
import net.logan.register.tickets.Ticket;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings("serial")
public class Register extends JFrame {
	
	Core core;
	
	public Register() {
		setAlwaysOnTop(true);
		
		core = new Core();
		
		this.setSize(852, 480);
		this.setTitle("Register");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setBackground(Color.DARK_GRAY);
		this.setType(Type.UTILITY);
		this.setResizable(false);
		getContentPane().setLayout(null);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				if (core.getCurrentUser() == null) return;
				core.getCurrentUser().setIsonline(false);
				core.getCurrentUser().save();
			}
		});
		
		loadMachineType();
	
		this.setVisible(true);
	}
	
	public void clearScreen()
	{
		this.getContentPane().removeAll();
		refreshScreen();
	}
	
	public void refreshScreen()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
	
	public Core getCore()
	{
		return core;
	}
	
	private void loadMachineType()
	{
		setContentPane(new typeGUI());
	}
	
	public void loadLoginUser()
	{
		this.setContentPane(new userGUI());
	}
	
	public void loadLoginPassword()
	{
		this.setContentPane(new passGUI());
	}
	
	public void login() {
		System.out.println("LOGIN SUCSESFUL");
		core.getCurrentUser().setIsonline(true);
		core.getCurrentUser().save();
		this.loadCurrentTicket();
		this.loadClientGUI();
	}
	
	public void loadClientGUI() {
		this.setContentPane(new clientGUI());
	}

	public void loadCurrentTicket()
	{
		boolean found = false;
		for (Ticket t : Main.tickMan.getAllTickets())
		{
			if (t.isCompleted() == false)
			{
				core.setCurrentTicket(t);
				found = true;
			}
		}
		if (found == false)
		{
			core.setCurrentTicket(new Ticket(core.getCurrentUser()));
		}
	}
	
	public void loadHostManager()
	{
		setContentPane(new hostGUI());
	}

	public void loadAccountManager() 
	{
		setContentPane(new accountManagerGUI());
	}

	public void loadNewUser() {
		setContentPane(new newUserGUI());
	}
	
	public void loadNewUserPass()
	{
		setContentPane(new newUserPassGUI());
		
	}

	public void loadPermissionsGUI() {
		setContentPane(new permissionsGUI());
	}

	public void loadItemManager() {
		setContentPane(new itemManagerGUI());
		
	}

	public void loadNewItem() {
		setContentPane(new newItemNameGUI());
		
	}
	
	public void loadNewItemPrice()
	{
		setContentPane(new newItemPriceGUI());
	}
}
