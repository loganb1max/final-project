package net.logan.register;

import java.util.ArrayList;

import net.logan.register.accounts.AccountManager;
import net.logan.register.items.ItemManager;
import net.logan.register.tickets.TicketManager;

public class Main {

	public static AccountManager accMan;
	public static TicketManager tickMan;
	public static ItemManager itemMan;
	public static Register reg;
	static ArrayList<String> defaultPerms = new ArrayList<String>();
	
	public static void main(String[] args) {
		
		reg = new Register();
		
		accMan = new AccountManager();
		itemMan = new ItemManager();
		tickMan = new TicketManager();
		
	}

}
