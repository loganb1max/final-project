package net.logan.register.accounts;

import java.util.ArrayList;

import net.logan.register.Main;

public class Account {

	private int accountID;
	private String userName;
	private String userPass;
	private ArrayList<String> permissions = new ArrayList<String>();
	private Boolean isonline;
	
	@SuppressWarnings("static-access")
	public Account(String user)
	{
		this.setAccountID(Main.accMan.getCurrentID());
		Main.accMan.updateCurrentID();
		this.setUserName(user);
		this.setIsonline(false);
		this.getPermissions().add("login");
	}
	public Account(int id, String user, String pass, ArrayList<String> perms, Boolean ison)
	{
		this.setAccountID(id);
		this.setUserName(user);
		this.setUserPass(pass);
		this.setPermissions(perms);
		this.setIsonline(ison);
	}
	public int getAccountID() {
		return this.accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public String getUserName() {
		return this.userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPass() {
		return this.userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public ArrayList<String> getPermissions() {
		return this.permissions;
	}
	public void setPermissions(ArrayList<String> permissions) {
		this.permissions = permissions;
	}
	public Boolean getIsonline() {
		return this.isonline;
	}
	public void setIsonline(Boolean isonline) {
		this.isonline = isonline;
	}
	
	public void save()
	{
		Main.accMan.saveAccount(this);
	}
	
	
	
	
	
}
