package net.logan.register.accounts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.logan.register.Config;
import net.logan.register.ConnectionPool;
import net.logan.register.Main;
import net.logan.register.items.Item;
@SuppressWarnings("unused")
public class AccountManager {

	public AccountManager(){}
	
	public  Account getAccount(String aUser)
	{
		
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT id, user, pass, permissions, isonline FROM Accounts WHERE user = '"
									+ aUser + "'");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			if (!(rs.next())) {
				return null;
			}
		
		int id = rs.getInt("id");
		String user = rs.getString("user");
		String pass = rs.getString("pass");
		String perms = rs.getString("permissions");
		Boolean isonline = rs.getBoolean("isonline");
		ArrayList<String> permsArray = new ArrayList<String>();
		for (String s : perms.substring(1, perms.length()-1).split(", "))
		{
			permsArray.add(s);
		}
		
		
		Account a = new Account(id, user, pass, permsArray, isonline);
		return a;	
		
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		return null;
	}
	
	public void saveAccount(Account a)
	{
		int id = a.getAccountID();
		String user = a.getUserName();
		String pass = a.getUserPass();
		String perms = a.getPermissions().toString();
		int isonline = 0;
		if (a.getIsonline() == true)
		{
			isonline = 1;
		}
		if (a.getIsonline() == false)
		{
			isonline = -1;
		}
		
		PreparedStatement pst = null;
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO Accounts "
														+ "(id, user, pass, permissions, isonline) "
														+ "VALUES('" + id
														+ "', '" + user
														+ "', '"+ pass
														+ "', '" + perms
														+ "', '" + isonline + "') "
														+ "ON DUPLICATE KEY UPDATE id='" + id
														+"', pass='" + pass
														+ "', permissions='" + perms 
														+ "', isonline='" + isonline + "'");
		
		pst.executeUpdate();	
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{
		
		}
		System.out.println("Account Saved!");
	}

	@SuppressWarnings("null")
	public static int getCurrentID() {
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT currentID FROM general");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			if (!(rs.next())) {
				return (Integer) null;
			}
		
		int id = rs.getInt("currentID");
		
		return id;	
		
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		return (Integer) null;
	}
	
	public List<String> getAllUsernames()
	{
		List<String> names = new ArrayList<String>();
		PreparedStatement pst = null;

		try {
			pst = ConnectionPool
					.getConnection()
					.prepareStatement(
							"SELECT user FROM Accounts order by user");

			pst.execute();
			ResultSet rs = pst.getResultSet();

			while (rs.next())
			{				
				names.add(rs.getString("user"));
			}
			
	}catch (Exception ex) {
		ex.printStackTrace();

	} finally {
		try {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}finally{
			
			}
		}
		System.out.println("Usernames loaded!");
		return names;
	}
	
	public static void deleteAccount(Account a)
	{
		PreparedStatement pst = null;
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `Register`.`Accounts` WHERE `Accounts`.`user` = \'" + a.getUserName() + "\'");
			pst.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally
		{
			
		}
		System.out.println("Account " + a.getUserName() + " Deleted!");
	}
	
	public static void deleteAccount(String username)
	{
		PreparedStatement pst = null;
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `Register`.`Accounts` WHERE `Accounts`.`user` = \'" + username + "\'");
			pst.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally
		{
			
		}
		System.out.println("Account " + username + " Deleted!");
	}
	
	
	@SuppressWarnings("static-access")
	public static void updateCurrentID()
	{
		int oldid = Main.accMan.getCurrentID();
		int oldnum= Main.tickMan.getCurrentNum();
		int id = oldid + 1;
		int num = oldnum +1;

		PreparedStatement pst = null;
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("DELETE FROM `Register`.`general` WHERE `general`.`currentID` = " + oldid);
			pst.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally
		{
			
		}
		
		
		
		try
		{
			pst = ConnectionPool.getConnection().prepareStatement("INSERT INTO general "
					+ "(currentID, currentNum) "
					+ "VALUES('" + id
					+ "', '" + oldnum
					+ "') "
					+ "ON DUPLICATE KEY UPDATE currentID='" + id
					+"', currentNum='" + oldnum + "'");
		
		pst.executeUpdate();	
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{
		
		}
	}
	
	
	
	
	
	
	
	
}
