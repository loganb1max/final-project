package net.logan.register;

import java.sql.Connection;
import java.sql.DriverManager;


public class ConnectionPool
{
	
	private static Connection con;
	
	public static boolean refresh = false;
	
	public static Connection getConnection()
	{
		try
		{
			if (refresh || con == null || con.isClosed())
			{
				refresh = false;
				if (con != null) con.close();
				con = DriverManager.getConnection(Config.getURL(), Config.getUser(), Config.getPass());
			}
		}
		catch (Exception e)
		{
			System.out.println("Could Not Connect To Database.");
			System.out.println(e);
		}
		return con;
	}
	
}
