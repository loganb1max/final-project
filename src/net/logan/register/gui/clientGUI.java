package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;

import net.logan.register.Main;
import net.logan.register.items.Item;
import net.logan.register.tickets.Ticket;

@SuppressWarnings("serial")
public class clientGUI extends Container{

	@SuppressWarnings("rawtypes")
	JList cart;
	@SuppressWarnings("rawtypes")
	JList options;
	JButton add;
	JButton remove;
	JButton cashout;
	JLabel subtotal;
	JLabel subtotaln;
	JLabel total;
	JLabel totaln;
	JLabel ticket;
	JLabel items;
	DecimalFormat format = new DecimalFormat("0.00");
	
	public clientGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
		Main.reg.refreshScreen();
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadComponents()
	{
		String[] cartNA = new String[Main.reg.getCore().getCurrentTicket().getItems().size()];
		for (int i = 0; i < cartNA.length; i++)
		{
			cartNA[i] = Main.reg.getCore().getCurrentTicket().getItems().get(i).toString();
		}
		cart = new JList(cartNA);
		cart.setBounds(50, 50, 300, 350);
		this.add(cart);
		
		
		String[] optionsNA = new String[Main.itemMan.getAllItems().size()];
		for (int i = 0; i < optionsNA.length; i++)
		{
			optionsNA[i] = Main.itemMan.getAllItems().get(i).toString();
		}
		options = new JList(optionsNA);
		options.setBounds(500, 50, 300, 350);
		this.add(options);
		
		add = new JButton("Add");
		add.setBounds(350, 100, 150, 30);
		add.setFont(new Font("Stencil", Font.BOLD, 20));
		add.setBackground(Color.LIGHT_GRAY);
		add.setForeground(Color.DARK_GRAY);
		add.setBorder(null);
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (options.isSelectionEmpty() == false)
				{
					if (!Main.reg.getCore().getCurrentTicket().getItems().contains(Main.itemMan.getItem(options.getSelectedValue().toString().split(" ")[0])))
					{
						Main.reg.getCore().getCurrentTicket().add(Main.itemMan.getItem(options.getSelectedValue().toString().split(" ")[0]));
						Main.tickMan.saveTicket(Main.reg.getCore().getCurrentTicket());
						Main.reg.clearScreen();
						Main.reg.loadClientGUI();
					}
					else
					{
						System.out.println("Ticket Already Contains " + Main.itemMan.getItem(options.getSelectedValue().toString().split(" ")[0]).toString());
					}
				}
			}
		});
		this.add(add);
		
		remove = new JButton("Remove");
		remove.setBounds(350, 150, 150, 30);
		remove.setFont(new Font("Stencil", Font.BOLD, 20));
		remove.setBackground(Color.LIGHT_GRAY);
		remove.setForeground(Color.DARK_GRAY);
		remove.setBorder(null);
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cart.isSelectionEmpty() == false)
				{
					Main.reg.getCore().getCurrentTicket().getItems().remove(Main.itemMan.getItem(cart.getSelectedValue().toString().split(" ")[0]));
					Main.tickMan.saveTicket(Main.reg.getCore().getCurrentTicket());
					Main.reg.clearScreen();
					Main.reg.loadClientGUI();
					System.out.println(Main.itemMan.getItem(cart.getSelectedValue().toString().split(" ")[0]).toString() + " Removed from cart!");
				}
			}
		});
		this.add(remove);
		
		subtotal = new JLabel("Subtotal:");
		subtotal.setBounds(350, 200, 150, 30);
		subtotal.setFont(new Font("Stencil", Font.BOLD, 20));
		subtotal.setForeground(Color.DARK_GRAY);
		subtotal.setBorder(null);
		subtotal.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(subtotal);
		
		subtotaln = new JLabel(format.format(getSubtotal()));
		subtotaln.setBounds(350, 220, 150, 30);
		subtotaln.setFont(new Font("Stencil", Font.BOLD, 20));
		subtotaln.setForeground(Color.DARK_GRAY);
		subtotaln.setBorder(null);
		subtotaln.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(subtotaln);
		
		total = new JLabel("Total:");
		total.setBounds(350, 260, 150, 30);
		total.setFont(new Font("Stencil", Font.BOLD, 20));
		total.setForeground(Color.DARK_GRAY);
		total.setBorder(null);
		total.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(total);
		
		totaln = new JLabel(format.format(getTotal()));
		totaln.setBounds(350, 280, 150, 30);
		totaln.setFont(new Font("Stencil", Font.BOLD, 20));
		totaln.setForeground(Color.DARK_GRAY);
		totaln.setBorder(null);
		totaln.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(totaln);
		
		cashout = new JButton("Cashout");
		cashout.setBounds(350, 320, 150, 30);
		cashout.setFont(new Font("Stencil", Font.BOLD, 20));
		cashout.setBackground(Color.LIGHT_GRAY);
		cashout.setForeground(Color.DARK_GRAY);
		cashout.setBorder(null);
		cashout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.getCore().getCurrentTicket().setCompleted(true);
				Main.tickMan.saveTicket(Main.reg.getCore().getCurrentTicket());
				Main.reg.getCore().setCurrentTicket(new Ticket(Main.reg.getCore().getCurrentUser())); 
				Main.reg.clearScreen();
				Main.reg.loadClientGUI();
			}
		});
		this.add(cashout);
		
		ticket = new JLabel("Ticket");
		ticket.setBounds(115, 20, 150, 30);
		ticket.setFont(new Font("Stencil", Font.BOLD, 20));
		ticket.setForeground(Color.DARK_GRAY);
		ticket.setBorder(null);
		ticket.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(ticket);
		
		items = new JLabel("Items");
		items.setBounds(575, 20, 150, 30);
		items.setFont(new Font("Stencil", Font.BOLD, 20));
		items.setForeground(Color.DARK_GRAY);
		items.setBorder(null);
		items.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(items);
		
	}
	
	public double getSubtotal()
	{
		double output = 0.00;
		
		for (Item i : Main.reg.getCore().getCurrentTicket().getItems())
		{
			output += i.getPrice();
		}
		
		return output;
	}
	
	public double getTotal()
	{
		double output = getSubtotal();
		
		output += (output*0.06);
		
		return output;
	}
	
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
