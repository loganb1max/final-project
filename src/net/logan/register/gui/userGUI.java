package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class userGUI extends Container{
	
	private JTextField usernameIn;
	JLabel lblUser = new JLabel("Username");
	private final JLabel lblErrorOut = new JLabel();

	
	public userGUI()
	{
		this.setBackground(Color.DARK_GRAY);
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	private void loadComponents()
	{
		lblErrorOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorOut.setFont(new Font("Stencil", Font.BOLD, 24));
		lblErrorOut.setForeground(Color.RED);
		lblErrorOut.setBounds(270, 300, 250, 36);
		
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setFont(new Font("Stencil", Font.BOLD, 24));
		lblUser.setForeground(Color.DARK_GRAY);
		lblUser.setBounds(290, 137, 200, 36);
		this.add(lblUser);
		
		usernameIn = new JTextField();
		usernameIn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
				{
					if (Main.accMan.getAccount(usernameIn.getText()) != null)
					{
						if (Main.reg.getCore().getType().equals("client"))
						{
							if (Main.accMan.getAccount(usernameIn.getText()).getPermissions().contains("login"))
							{
								Main.reg.getCore().setCurrentUser(Main.accMan.getAccount(usernameIn.getText()));
								Main.reg.clearScreen();
								Main.reg.loadLoginPassword();
							}
							else if (!Main.accMan.getAccount(usernameIn.getText()).getPermissions().contains("login"))
							{
								lblErrorOut.setText("No Permission!");
								add(lblErrorOut);
								refresh();
							}
						}
						else if (Main.reg.getCore().getType().equals("host"))
						{
							if (Main.accMan.getAccount(usernameIn.getText()).getPermissions().contains("host"))
							{
								Main.reg.getCore().setCurrentUser(Main.accMan.getAccount(usernameIn.getText()));
								Main.reg.clearScreen();
								Main.reg.loadLoginPassword();
							}
							else if (!Main.accMan.getAccount(usernameIn.getText()).getPermissions().contains("host"))
							{
								lblErrorOut.setText("No Permission!");
								add(lblErrorOut);
								refresh();
							}
						}
					}
					if (Main.accMan.getAccount(usernameIn.getText()) == null)
					{
						lblErrorOut.setText("User Not Found!");
						add(lblErrorOut);
						refresh();
						
					}
				}
			}
		});
		usernameIn.setBounds(290, 184, 200, 36);
		this.add(usernameIn);
		usernameIn.setColumns(10);
		
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
	
}
