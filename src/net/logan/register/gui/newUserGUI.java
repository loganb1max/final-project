package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.logan.register.Main;
import net.logan.register.accounts.Account;

@SuppressWarnings("serial")
public class newUserGUI extends Container{
	
	private JTextField usernameIn;
	JLabel lblUser = new JLabel("Username");
	private final JLabel lblErrorOut = new JLabel();

	
	public newUserGUI()
	{
		this.setBackground(Color.DARK_GRAY);
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	private void loadComponents()
	{
		lblErrorOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorOut.setFont(new Font("Stencil", Font.BOLD, 24));
		lblErrorOut.setForeground(Color.RED);
		lblErrorOut.setBounds(270, 300, 250, 36);
		
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setFont(new Font("Stencil", Font.BOLD, 24));
		lblUser.setForeground(Color.DARK_GRAY);
		lblUser.setBounds(290, 137, 200, 36);
		this.add(lblUser);
		
		usernameIn = new JTextField();
		usernameIn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
				{
					if (usernameIn.getText().equals("") || usernameIn.getText().contains(" "))
					{
						lblErrorOut.setText("Invaild Username");
						add(lblErrorOut);
						usernameIn.setText("");
						refresh();
					}
					else if (Main.accMan.getAllUsernames().contains(usernameIn.getText()))
					{
						lblErrorOut.setText("User Exists");
						add(lblErrorOut);
						usernameIn.setText("");
						refresh();
					}
					else
					{
						Main.reg.getCore().setCurrentUser(new Account(usernameIn.getText()));
						Main.reg.clearScreen();
						Main.reg.loadNewUserPass();
					}
					
				}
			}
		});
		usernameIn.setBounds(290, 184, 200, 36);
		this.add(usernameIn);
		usernameIn.setColumns(10);
		
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
	
}
