package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class permissionsGUI extends Container{

	JCheckBox login;
	JCheckBox host;
	JButton save;
	
	public permissionsGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	
	private void loadComponents() 
	{
		login = new JCheckBox("Login");
		login.setSelected(Main.reg.getCore().getCurrentUser().getPermissions().contains("login"));
		login.setBounds(150, 50, 300, 30);
		this.add(login);
		
		host = new JCheckBox("Host");
		host.setSelected(Main.reg.getCore().getCurrentUser().getPermissions().contains("host"));
		host.setBounds(450, 50, 300, 30);
		this.add(host);
		
		
		save = new JButton("Save");
		save.setBounds(300, 100, 200, 30);
		save.setFont(new Font("Stencil", Font.BOLD, 20));
		save.setBackground(Color.LIGHT_GRAY);
		save.setForeground(Color.DARK_GRAY);
		save.setBorder(null);
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> sPerms = new ArrayList<String>();
				if (login.isSelected() == true) sPerms.add("login");
				if (host.isSelected() == true) sPerms.add("host");
				Main.reg.getCore().getCurrentUser().setPermissions((ArrayList<String>) sPerms);
				Main.reg.getCore().getCurrentUser().save();
				Main.reg.clearScreen();
				Main.reg.loadAccountManager();
				refresh();
			}
		});
		this.add(save);
	}


	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
