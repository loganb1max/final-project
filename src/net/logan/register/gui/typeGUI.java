package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class typeGUI extends Container{
	
	JLabel lblIsThisA = new JLabel("Is This a Host or a Client?");
	JButton btnHost = new JButton("HOST");
	JButton btnClient = new JButton("CLIENT");
	
	
	public typeGUI()
	{
		setBackground(Color.DARK_GRAY);
		setLayout(null);
		loadComponents();
		refresh();
	}

	
	private void loadComponents()
	{
		lblIsThisA.setForeground(Color.LIGHT_GRAY);
		lblIsThisA.setFont(new Font("Stencil", Font.BOLD, 24));
		lblIsThisA.setHorizontalAlignment(SwingConstants.CENTER);
		lblIsThisA.setBackground(Color.LIGHT_GRAY);
		lblIsThisA.setBorder(null);
		lblIsThisA.setBounds(0, 0, 846, 25);
		this.add(lblIsThisA);
		
		btnHost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.reg.getCore().setType("host");
				Main.reg.clearScreen();
				Main.reg.loadLoginUser();
			}
		});
		btnHost.setBorder(null);
		btnHost.setFont(new Font("Stencil", Font.BOLD, 24));
		btnHost.setBackground(Color.LIGHT_GRAY);
		btnHost.setForeground(Color.DARK_GRAY);
		btnHost.setBounds(60, 95, 300, 200);
		this.add(btnHost);
		
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.getCore().setType("client");
				Main.reg.clearScreen();
				Main.reg.loadLoginUser();
			}
		});
		btnClient.setBorder(null);
		btnClient.setBackground(Color.LIGHT_GRAY);
		btnClient.setForeground(Color.DARK_GRAY);
		btnClient.setFont(new Font("Stencil", Font.BOLD, 24));
		btnClient.setBounds(480, 95, 300, 200);
		this.add(btnClient);
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
