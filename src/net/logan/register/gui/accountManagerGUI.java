package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JList;

import net.logan.register.Main;

@SuppressWarnings({ "rawtypes", "serial" })
public class accountManagerGUI extends Container{

	
	JList accountsDisplay;
	JButton add = new JButton("Add Account");
	JButton delete = new JButton("Delete Account");
	JButton perms = new JButton("Permissions");
	JButton pass = new JButton("Edit Password");
	JButton back = new JButton("Back");
	
	public accountManagerGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	
	@SuppressWarnings("unchecked")
	private void loadComponents()
	{
		List<String> name = Main.accMan.getAllUsernames();
		String[] names = new String[name.size()];
		for (int i = 0; i < names.length; i++)
		{
			names[i] = name.get(i);
		}
		accountsDisplay = new JList(names);
		accountsDisplay.setBounds(50, 50, 250, 350);
		this.add(accountsDisplay);
		
		add.setBounds(350, 50, 200, 30);
		add.setFont(new Font("Stencil", Font.BOLD, 20));
		add.setBackground(Color.LIGHT_GRAY);
		add.setForeground(Color.DARK_GRAY);
		add.setBorder(null);
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadNewUser();
			}
		});
		this.add(add);
		
		delete.setBounds(600, 50, 200, 30);
		delete.setFont(new Font("Stencil", Font.BOLD, 20));
		delete.setBackground(Color.LIGHT_GRAY);
		delete.setForeground(Color.DARK_GRAY);
		delete.setBorder(null);
		delete.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				Main.accMan.deleteAccount((String)accountsDisplay.getSelectedValue());
				Main.reg.clearScreen();
				Main.reg.loadAccountManager();
				refresh();
			}
		});
		this.add(delete);
		
		pass.setBounds(350, 100, 200, 30);
		pass.setFont(new Font("Stencil", Font.BOLD, 20));
		pass.setBackground(Color.LIGHT_GRAY);
		pass.setForeground(Color.DARK_GRAY);
		pass.setBorder(null);
		pass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!(accountsDisplay.getSelectedValue() == null))
				{
					Main.reg.getCore().setCurrentUser(Main.accMan.getAccount((String)accountsDisplay.getSelectedValue()));
					Main.reg.clearScreen();
					Main.reg.loadNewUserPass();
					refresh();
				}
			}
		});
		this.add(pass);
		
		perms.setBounds(600, 100, 200, 30);
		perms.setFont(new Font("Stencil", Font.BOLD, 20));
		perms.setBackground(Color.LIGHT_GRAY);
		perms.setForeground(Color.DARK_GRAY);
		perms.setBorder(null);
		perms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!(accountsDisplay.getSelectedValue() == null))
				{
					Main.reg.getCore().setCurrentUser(Main.accMan.getAccount((String)accountsDisplay.getSelectedValue()));
					Main.reg.clearScreen();
					Main.reg.loadPermissionsGUI();
					refresh();
				}
			}
		});
		this.add(perms);
		
		back.setBounds(600, 400, 200, 30);
		back.setFont(new Font("Stencil", Font.BOLD, 20));
		back.setBackground(Color.LIGHT_GRAY);
		back.setForeground(Color.DARK_GRAY);
		back.setBorder(null);
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadHostManager();
			}
		});
		this.add(back);
	}

	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
