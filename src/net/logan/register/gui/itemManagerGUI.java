package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class itemManagerGUI extends Container{

	@SuppressWarnings("rawtypes")
	JList itemDisplay;
	JButton add = new JButton("Add Item");
	JButton delete = new JButton("Delete Item");
	JButton back = new JButton("Back");
	
	public itemManagerGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void loadComponents()
	{
		String[] names = new String[Main.itemMan.getAllItems().size()];
		for (int i = 0; i < names.length; i++)
		{
			names[i] = Main.itemMan.getAllItems().get(i).toString();
		}
		itemDisplay = new JList(names);
		itemDisplay.setBounds(50, 50, 250, 350);
		this.add(itemDisplay);
		
		add.setBounds(350, 50, 200, 30);
		add.setFont(new Font("Stencil", Font.BOLD, 20));
		add.setBackground(Color.LIGHT_GRAY);
		add.setForeground(Color.DARK_GRAY);
		add.setBorder(null);
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadNewItem();
			}
		});
		this.add(add);
		
		delete.setBounds(600, 50, 200, 30);
		delete.setFont(new Font("Stencil", Font.BOLD, 20));
		delete.setBackground(Color.LIGHT_GRAY);
		delete.setForeground(Color.DARK_GRAY);
		delete.setBorder(null);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (itemDisplay.isSelectionEmpty() == false)
				{
					Main.itemMan.deleteItem(Main.itemMan.getItem(itemDisplay.getSelectedValue().toString().split(" ")[0]));
					Main.reg.clearScreen();
					Main.reg.loadItemManager();
					refresh();
				}
			}
		});
		this.add(delete);
		
		back.setBounds(600, 400, 200, 30);
		back.setFont(new Font("Stencil", Font.BOLD, 20));
		back.setBackground(Color.LIGHT_GRAY);
		back.setForeground(Color.DARK_GRAY);
		back.setBorder(null);
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadHostManager();
			}
		});
		this.add(back);
	}

	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
