package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class newItemNameGUI extends Container{
	
	private JTextField itemnameIn;
	JLabel lblName = new JLabel("Item Name");
	private final JLabel lblErrorOut = new JLabel();
	
	public newItemNameGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
	}

	private void loadComponents()
	{
		lblErrorOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorOut.setFont(new Font("Stencil", Font.BOLD, 24));
		lblErrorOut.setForeground(Color.RED);
		lblErrorOut.setBounds(270, 300, 250, 36);
		
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setFont(new Font("Stencil", Font.BOLD, 24));
		lblName.setForeground(Color.DARK_GRAY);
		lblName.setBounds(290, 137, 200, 36);
		this.add(lblName);
		
		itemnameIn = new JTextField();
		itemnameIn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
				{
					if (itemnameIn.getText().equals("") || itemnameIn.getText().contains(" "))
					{
						lblErrorOut.setText("Invalid Name");
						add(lblErrorOut);
						itemnameIn.setText("");
						refresh();
					}
					else if (Main.itemMan.getNames().contains(itemnameIn.getText().toLowerCase()))
					{
						lblErrorOut.setText("User Exists");
						add(lblErrorOut);
						itemnameIn.setText("");
						refresh();
					}
					else
					{
						Main.reg.getCore().setTempItemName(itemnameIn.getText());
						Main.reg.clearScreen();
						Main.reg.loadNewItemPrice();
					}
					
				}
			}
		});
		itemnameIn.setBounds(290, 184, 200, 36);
		this.add(itemnameIn);
		itemnameIn.setColumns(10);
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
