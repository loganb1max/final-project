package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.logan.register.Main;
import net.logan.register.items.Item;

@SuppressWarnings("serial")
public class newItemPriceGUI extends Container{

	private JTextField itempriceIn;
	JLabel lblPrice = new JLabel("Item Price");
	private final JLabel lblErrorOut = new JLabel();
	
	public newItemPriceGUI()
	{
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	private void loadComponents()
	{
		lblErrorOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorOut.setFont(new Font("Stencil", Font.BOLD, 24));
		lblErrorOut.setForeground(Color.RED);
		lblErrorOut.setBounds(270, 300, 250, 36);
		
		lblPrice.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrice.setFont(new Font("Stencil", Font.BOLD, 24));
		lblPrice.setForeground(Color.DARK_GRAY);
		lblPrice.setBounds(290, 137, 200, 36);
		this.add(lblPrice);
		
		itempriceIn = new JTextField();
		itempriceIn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
				{
					if (itempriceIn.getText().equals("") || itempriceIn.getText().contains(" ") || isDouble(itempriceIn.getText()) == false)
					{
						lblErrorOut.setText("Invalid Price");
						add(lblErrorOut);
						itempriceIn.setText("");
						refresh();
					}
					else
					{
						Main.itemMan.addItem(new Item(Main.reg.getCore().getTempItemName(), Double.parseDouble(itempriceIn.getText())));
						Main.reg.clearScreen();
						System.out.println(Main.itemMan.getItem(Main.reg.getCore().getTempItemName()) + " Created!");
						Main.reg.loadItemManager();
					}
					
				}
			}
		});
		itempriceIn.setBounds(290, 184, 200, 36);
		this.add(itempriceIn);
		itempriceIn.setColumns(10);
	}
	
	private boolean isDouble(String s)
	{
		try
		{
		  Double.parseDouble(s);
		  return true;
		}
		catch(NumberFormatException e)
		{
		  return false;
		}
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
