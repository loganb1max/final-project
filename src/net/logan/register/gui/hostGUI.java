package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class hostGUI extends Container{
	
	private JButton manAccounts = new JButton();
	private JButton manItems = new JButton();
	

	public hostGUI()
	{
		this.setBackground(Color.DARK_GRAY);
		this.setLayout(null);
		loadComponents();
		refresh();
	}
	
	private void loadComponents()
	{
		manAccounts.setBounds(50, 50, 200, 50);
		manAccounts.setText("Manage Accounts");
		manAccounts.setFont(new Font("Stencil", Font.BOLD, 20));
		manAccounts.setBackground(Color.LIGHT_GRAY);
		manAccounts.setForeground(Color.DARK_GRAY);
		manAccounts.setBorder(null);
		manAccounts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadAccountManager();
			}
		});
		this.add(manAccounts);
		
		manItems.setBounds(350, 50, 200, 50);
		manItems.setText("Manage Items");
		manItems.setFont(new Font("Stencil", Font.BOLD, 20));
		manItems.setBackground(Color.LIGHT_GRAY);
		manItems.setForeground(Color.DARK_GRAY);
		manItems.setBorder(null);
		manItems.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.reg.clearScreen();
				Main.reg.loadItemManager();
			}
		});
		this.add(manItems);
	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
}
