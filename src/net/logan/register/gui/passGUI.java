package net.logan.register.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.logan.register.Main;

@SuppressWarnings("serial")
public class passGUI extends Container{
	
	private JTextField passIn;
	JLabel lblPass = new JLabel("Password");
	private final JLabel lblErrorOut = new JLabel();

	
	public passGUI()
	{
		setBackground(Color.DARK_GRAY);
		setLayout(null);
		loadComponents();
		refresh();
	}

	private void loadComponents()
	{
		lblErrorOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorOut.setFont(new Font("Stencil", Font.BOLD, 24));
		lblErrorOut.setForeground(Color.RED);
		lblErrorOut.setBounds(270, 300, 250, 36);
		
		lblPass.setHorizontalAlignment(SwingConstants.CENTER);
		lblPass.setFont(new Font("Stencil", Font.BOLD, 24));
		lblPass.setForeground(Color.DARK_GRAY);
		lblPass.setBounds(290, 137, 200, 36);
		this.add(lblPass);
		
		passIn = new JTextField();
		passIn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
				{
					if (passIn.getText().equals(Main.reg.getCore().getCurrentUser().getUserPass()))
					{
						Main.reg.clearScreen();
						if (Main.reg.getCore().getType().equals("client"))
						{
							Main.reg.login();
						}
						if (Main.reg.getCore().getType().equals("host"))
						{
							Main.reg.loadHostManager();
						}
					}
					if (!passIn.getText().equals(Main.reg.getCore().getCurrentUser().getUserPass()))
					{
						lblErrorOut.setText("INCORRECT PASSWORD!");
						passIn.setText("");
						add(lblErrorOut);
						refresh();
					}
				}
			}
		});
		passIn.setBounds(290, 184, 200, 36);
		this.add(passIn);
		passIn.setColumns(10);

	}
	
	public void refresh()
	{
		for (Component c : this.getComponents())
		{
			c.setVisible(false);
			c.setVisible(true);
		}
	}
	
}
